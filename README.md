## Using the project

Use `docker-compose up` to bring the services up.

Run tests using `docker-compose run tests ./run_tests.sh`

## API

**POST** `/predict` - accepts multipart/form-data with image `file` of
at most 5 Mb. Returns JSON object with single property `task_id'
with value equal to task uuid.

Response example:
```json
{
    "task_id": "f2f2d136-cc95-4eb5-8c1a-070f1522471c"
}
```

**GET** `/results/<uuid:task_uuid>` - returns results of the task
by task-id (receive in response to `/predict`).

Response example:
```json
{
    "task_id": "24d90834-9723-4cdf-9ce6-2474c2c19352",
    "status": "complete",
    "class": [
        "n03141823",
        "crutch"
    ]
}
```