#!/bin/bash

./wait-for-it.sh -h redis -p 6379 -t 60
gunicorn -b 0.0.0.0:$SERVICE_PORT 'app:create_app()'