import json

from flask import Response
from werkzeug.exceptions import HTTPException

from exceptions import ApiError


def json_response(data, status=200):
    return Response(
        json.dumps(data),
        content_type='application/json; charset=UTF8',
        status=status
    )


class JSONResponse(Response):
    @classmethod
    def force_type(cls, rv, environ=None):
        if isinstance(rv, (dict, list)):
            rv = json_response(rv)
        return super(JSONResponse, cls).force_type(rv, environ)


def json_error_handler(error):
    if isinstance(error, HTTPException):
        message = error.description
        status_code = error.code
    elif isinstance(error, ApiError):
        message = error.message
        status_code = error.status_code
    else:
        raise NotImplementedError()
    return {'error': message}, status_code
