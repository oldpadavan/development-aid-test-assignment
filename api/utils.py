import io

from torchvision import transforms
from PIL import Image, UnidentifiedImageError

import exceptions


image_transformation = transforms.Compose([
    transforms.Resize(256),
    transforms.CenterCrop(224),
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
])


def pre_process_image(image):
    """ Performs image pre-processing - converts to 3 channel RGB,
    resizes, normalizes, returning Tensor
    """
    try:
        image = Image.open(io.BytesIO(image))
    except UnidentifiedImageError:
        raise exceptions.InvalidImageFile()
    image = image.convert('RGB')
    return image_transformation(image).unsqueeze(0)
