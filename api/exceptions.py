class ApiError(Exception):
    status_code = 400
    message = 'undefined'


class FileExpectedError(ApiError):
    status_code = 400
    message = 'file was expected'


class InvalidImageFileExtension(ApiError):
    status_code = 400
    message = 'invalid file extension'


class InvalidImageFile(ApiError):
    status_code = 400
    message = 'invalid image'


class TaskNotFound(ApiError):
    status_code = 404
    message = 'Task not found'
