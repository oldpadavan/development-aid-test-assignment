import json
import pickle
import uuid

from flask import current_app, Blueprint, request

import exceptions
import utils
from app import redis_client


predict_bp = Blueprint('predict', __name__)


def is_file_name_valid(file_name):
    """ Returns boolean value indicating whether image filename extension is supported
    """
    return (
        '.' in file_name and
        file_name.rsplit('.', 1)[1].lower() in current_app.config['ALLOWED_EXTENSIONS']
    )


@predict_bp.route('/predict', methods=['POST'])
def create_prediction_job():
    if 'file' not in request.files:
        raise exceptions.FileExpectedError()

    file = request.files['file']
    if not is_file_name_valid(file.filename):
        raise exceptions.InvalidImageFileExtension()

    image = utils.pre_process_image(file.read())
    image_uuid = str(uuid.uuid4())
    redis_client.publish(
        current_app.config['PUB_SUB_CHANNEL'],
        pickle.dumps({
            'task_id': image_uuid,
            'image': image.numpy()
        }))
    return {'task_id': image_uuid}


@predict_bp.route('/results/<uuid:task_uuid>', methods=['GET'])
def get_job_status(task_uuid):
    task_id = str(task_uuid)
    results = redis_client.get(task_id)
    if not results:
        raise exceptions.TaskNotFound()
    results = json.loads(results)
    return {
        'task_id': task_id,
        **results
    }
