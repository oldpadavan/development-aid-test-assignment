class Config:
    REDIS_URL = 'redis://redis'
    MAX_CONTENT_LENGTH = 5 * 1024 * 1024  # 5 Mb
    ALLOWED_EXTENSIONS = ('jpg', 'jpe', 'jpeg', 'png', 'gif', 'svg', 'bmp', 'webp')
    PUB_SUB_CHANNEL = 'tasks'


class TestingConfig(Config):
    TESTING = True
