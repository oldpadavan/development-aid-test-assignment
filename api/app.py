from flask import Flask
from werkzeug.exceptions import HTTPException
from flask_redis import FlaskRedis

import response
import exceptions


redis_client = FlaskRedis()


def create_app(config='config.Config'):
    app = Flask(__name__)
    app.config.from_object(config)

    redis_client.init_app(app)
    app.response_class = response.JSONResponse
    app.register_error_handler(
        exceptions.ApiError, response.json_error_handler)
    app.register_error_handler(
        HTTPException, response.json_error_handler)

    from views.predict import predict_bp
    app.register_blueprint(predict_bp, url_prefix='/')

    return app
