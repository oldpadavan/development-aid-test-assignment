Flask==1.0.2
redis==3.5.2
torch==1.5.0
torchvision==0.6.0
flask-redis==0.4.0
gunicorn==19.7.0