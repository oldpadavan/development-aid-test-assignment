from unittest.mock import patch

import pytest

from app import create_app


@pytest.fixture(autouse=True)
def app():
    with patch('app.redis_client'):
        app = create_app('config.TestingConfig')
        req_ctx = app.test_request_context()
        req_ctx.push()
        yield app
        req_ctx.pop()


@pytest.fixture
def client(app):
    ctx = app.app_context()
    ctx.push()
    with app.test_client() as client:
        yield client
    ctx.pop()
