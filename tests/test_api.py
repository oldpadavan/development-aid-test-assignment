import io
import os
import pickle
from unittest.mock import patch

import pytest
from flask import url_for

import config
import exceptions


DIR_PATH = os.path.dirname(os.path.abspath(__file__))


@pytest.fixture
def redis_client():
    with patch('views.predict.redis_client') as redis_mock:
        yield redis_mock


def test_predict_wo_file_fails(client):
    r = client.post(url_for('predict.create_prediction_job'))
    assert r.status_code == 400
    assert r.json['error'] == exceptions.FileExpectedError.message


def test_predict_invalid_file_extension_fails(client):
    r = client.post(
        url_for('predict.create_prediction_job'),
        data={'file': (io.BytesIO(b'abcdef'), 'test.txt')},
        content_type='multipart/form-data'
    )
    assert r.status_code == 400
    assert r.json['error'] == exceptions.InvalidImageFileExtension.message


def test_predict_invalid_image_file_fails(client):
    r = client.post(
        url_for('predict.create_prediction_job'),
        data={'file': (io.BytesIO(b'abcdef'), 'test.jpeg')},
        content_type='multipart/form-data'
    )
    assert r.status_code == 400
    assert r.json['error'] == exceptions.InvalidImageFile.message


def test_predict_valid_file(client, redis_client):
    with open(os.path.join(DIR_PATH, 'dog_jackson.png'), 'rb') as f:
        r = client.post(
            url_for('predict.create_prediction_job'),
            data={'file': (f, 'test.jpeg')},
            content_type='multipart/form-data'
        )
    assert r.status_code == 200
    args, _ = redis_client.publish.call_args
    assert args[0] == config.Config.PUB_SUB_CHANNEL
    task_data = pickle.loads(args[1])
    assert 'task_id' in task_data
    assert 'image' in task_data
