#!/bin/bash

./wait-for-it.sh -h redis -p 6379 -t 60
./wait-for-it.sh -h api -p 8080 -t 60
python classifier.py