import json
import logging
import pickle

import redis
import torch
import torchvision
import torch.multiprocessing as mp
import queue


NUMBER_OF_WORKERS = 4
REDIS_HOST = 'redis'
PUB_SUB_CHANNEL_NAME = 'tasks'
IMAGE_NET_FILE_PATH = './imagenet_class_index.json'

with open(IMAGE_NET_FILE_PATH) as f:
    imagenet_class_index = json.load(f)


def get_logger():
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    log_formatter = logging.Formatter(
        "%(asctime)s [%(levelname)-5.5s]  %(message)s")
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(log_formatter)
    logger.addHandler(console_handler)
    return logger


class Worker(mp.Process):
    """ Worker process performing predictions - runs in infinite loop
    waiting for tasks coming in task queue and putting results in result queue
    """
    def __init__(self, model, task_queue, result_queue):
        """ Initializes Worker instance
        :param model: pytorch model instance
        :param task_queue: Task queue where each task is pickled dict with
        keys task_id (task uuid) and image (Tensor)
        :param result_queue: Results queue where each result is a tuple where
        first item is task_id and the second - predicted class, which is a list
        of 2 elements (imagenet idx and label). If class is unrecognised - returns
        [None, None]
        """
        super(Worker, self).__init__()
        self.model = model
        self.task_queue = task_queue
        self.result_queue = result_queue

    def run(self):
        while True:
            try:
                data = self.task_queue.get_nowait()
            except queue.Empty:
                continue
            tensor = torch.from_numpy(data['image'])
            outputs = self.model.forward(tensor)
            _, y_hat = outputs.max(1)
            predicted_idx = str(y_hat.item())
            imagenet_class = imagenet_class_index.get(predicted_idx, [None, None])
            self.result_queue.put((data['task_id'], imagenet_class))


def run():
    """Classifier service

    Subscribes to API PubSub notifications for new tasks, submits
    incoming tasks to a pool of process workers, reads task results from
    result queue and writes them to Redis
    """
    redis_client = redis.Redis(REDIS_HOST)
    tasks_sub = redis_client.pubsub()
    tasks_sub.subscribe(PUB_SUB_CHANNEL_NAME)
    task_queue = mp.Queue()
    result_queue = mp.Queue()
    for i in range(NUMBER_OF_WORKERS):
        Worker(model, task_queue, result_queue).start()

    logger = get_logger()

    while True:
        message = tasks_sub.get_message()
        if message and message['type'] == 'message':
            data = pickle.loads(message['data'])
            logger.info(f'Got new task {data["task_id"]}')
            task_queue.put(data)
            redis_client.set(data['task_id'], json.dumps({
                'status': 'in progress',
                'class': None
            }))
        try:
            task_id, result = result_queue.get(block=False)
        except queue.Empty:
            pass
        else:
            logger.info(f'Completed task {task_id}')
            redis_client.set(task_id, json.dumps({
                'status': 'complete',
                'class': result
            }))


if __name__ == '__main__':
    mp.set_start_method('spawn', force=True)
    model = torchvision.models.resnet18(pretrained=True)
    model.share_memory()
    model.eval()
    run()
